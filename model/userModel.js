const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please tell us your name!']
    },
    email: {
        type: String,
        required: [true, 'Please tell us your email!'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please provide a valid mail'],
    },
    photo: {
        type: String,
        default: 'default.jpg'
    },
    role: {
        type: String,
        enum: ['user', 'sme', 'pharmacist', 'admin'],
        default: 'user',
    },
    password: {
        type: String,
        required: [true, 'Plaese provide password!'],
        select: false
    },
    active: {
        type : Boolean,
        default: true,
        select: false
    },
    passwordConfirm: {
        type: String,
        required: [true, 'Please confirm your password'],
        validator: function(el) {
            return el === this.password
        },
        message: 'Passwords are not the same',
    },
})

userSchema.pre('save', async function (next) {
        //ony run this function if password was actually modified
        if (!this.isModified('password')) return next()

        //hash the password with cost of 12
        this.password = await bcrypt.hash(this.password, 12)

        //delete passwordConfirm field
        this.passwordConfirm = undefined
        next()
})

userSchema.pre('findOneAndUpdate', async function (next) {
    const update = this.getUpdate();
    if (update.password !== '' &&
    update.password !== undefined &&
    update.password == update.passwordConfirm) {
        //hash the password w cost of 12
        this.getUpdate().password = await bcrypt.hash(update.password, 12)

        // delete passwordconfrim field
        update.passwordConfirm = undefined
        next()
    } else {
        next()
    }
})

userSchema.methods.correctPassword = async function (
    candidatePasssword, userPassword
) {
    return await bcrypt.compare(candidatePasssword, userPassword)
}

const User = mongoose.model('User', userSchema)
module.exports = User